# Simplon Grenoble

![Simplon alt](https://cibul.s3.amazonaws.com/89cc526f1ce241b4be454307db254090.base.image.jpg)

Création d'un mini site pour la fabrique numérique de Simplon Grenoble.

## Pour commencer

Aucune maquette graphique n'a été définie pour le site, nous avons carte blanche.

Le site devra être réalisé en HTML et CSS.

### Pré-requis


- Le site doit être responsive
- Nous utilisons SASS pour la création de notre CSS
- Nous Utilisons une convention de nommage
- Le site doit contenir un formulaire de contact, non fonctionnel mais les champs sont validés :
  - Nom
  - Prenom
  - Genre (Mr / Mme / Autre)
  - Email
  - Message (max 300 caractères)
- Les images doivent être optimisées pour le web (taille adaptée / poids etc)
- Les champs dédiés au référencement doivent être complétés (images et meta)
- Nous utilisons un framework CSS
- Nous utilisons toutes les librairies CSS ou JS que nous voulons


### Livrables


Un dépot Gitlab contenant le code source (HTML, SASS, CSS, Images, ...), ainsi que le schéma de notre maquette graphique. Ce schéma doit être au format image (JPG, PNG) et peut être fait soit sur un outil numérique ou alors juste en mode papier/crayon. 


## Fabriqué avec

* [Bootstrap](https://getbootstrap.com/) - Framework CSS (front-end)
* [VisualStudioCode](https://code.visualstudio.com/) - Editeur de textes

## Versions

**Dernière version stable :** 5.0
**Dernière version :** 5.1

## Auteurs

* **Nadjib Chaibeddra** 
* **Christophe Grava** 


## License

Ce projet est sous licence ``WTFTPL`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations
